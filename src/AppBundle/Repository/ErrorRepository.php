<?php
/**
 * Created by PhpStorm.
 * User: rondra
 * Date: 28. 10. 2015
 * Time: 11:17
 */

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\Error;

class ErrorRepository extends EntityRepository
{
    use RequestLimitsTrait;

    /**
     *  Find all records
     *
     * @return QueryBuilder
     */
    public function createQuery()
    {
        return $this->createQueryBuilder("p");
    }
}