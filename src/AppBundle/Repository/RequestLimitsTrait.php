<?php
/**
 * Created by PhpStorm.
 * User: rondra
 * Date: 14. 1. 2016
 * Time: 17:51
 */

namespace AppBundle\Repository;

use Doctrine\ORM\QueryBuilder;
use Symfony\Component\HttpFoundation\Request;

trait RequestLimitsTrait
{
    /**
     * Creates a new QueryBuilder instance that is prepopulated for this entity name.
     *
     * @see \Doctrine\ORM\QueryBuilder::createQueryBuilder()
     *
     * @param string $alias
     * @param string $indexBy The index for the from.
     *
     * @return QueryBuilder
     */
    abstract public function createQueryBuilder($alias, $indexBy = null);

    /**
     * @param Request $request
     * @param string $alias
     * @param int $total
     * @return mixed
     */
    public function createRequestQuery(Request $request, $alias, &$total)
    {
        $queryBuilder = $this->createQueryBuilder($alias);
        $this->applyRequestLimits($queryBuilder, $request, $alias, $total);
        return $queryBuilder->getQuery();
    }

    public function applyRequestLimits(QueryBuilder $queryBuilder, Request $request, $alias, &$total)
    {
        if ($request->query->has('sort')) {
            $column = $request->query->get('sort');
            $order = $request->query->has('order') && strtolower($request->query->get('order')) == 'desc' ? 'DESC' : 'ASC';
            $queryBuilder->addOrderBy($alias . '.' . $column, $order);
        } else {
            $queryBuilder->addOrderBy($alias . '.id', 'DESC');
        }

        $countExpression = sprintf('count(%s.id)', $alias);
        $qbCount = clone $queryBuilder;
        $total = $qbCount->select($countExpression)->getQuery()->getSingleScalarResult();

        if ($request->query->has('limit') && $limit = $request->query->get('limit')) {
            $queryBuilder->setMaxResults($limit);
        }

        if ($request->query->has('offset') && $offset = $request->query->get('offset')) {
            $queryBuilder->setFirstResult($offset);
        }

        return $queryBuilder;
    }
}
