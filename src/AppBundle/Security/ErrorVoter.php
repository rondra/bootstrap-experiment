<?php
/**
 * Created by PhpStorm.
 * User: rondra
 * Date: 16. 12. 2015
 * Time: 14:26
 */

namespace AppBundle\Security;


use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use AppBundle\Entity\Error;
use AppBundle\Entity\User;


class ErrorVoter extends Voter
{
    const VIEW = 'view';
    const EDIT = 'edit';
    const DELETE = 'delete';

    protected function supports($attribute, $subject)
    {
        // if the attribute isn't one we support, return false
        if (!in_array($attribute, array(self::VIEW, self::EDIT, self::DELETE))) {
            return false;
        }

        // only vote on Error objects inside this voter
        if (!$subject instanceof Error) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            // the user must be logged in; if not, deny access
            return false;
        }

        // you know $subject is a Error object, thanks to supports
        /** @var Error $error */
        $error = $subject;

        switch($attribute) {
            case self::VIEW:
                return $this->canView($error, $user);
            case self::EDIT:
                return $this->canEdit($error, $user);
            case self::DELETE:
                return $this->canDelete($error, $user);
        }
    }

    private function canView(Error $error, User $user)
    {
        // if they can edit, they can view
        if ($this->canEdit($error, $user)) {
            return true;
        }

        // the Post object could have, for example, a method isPrivate()
        // that checks a boolean $private property
        return $error->getIsActive();
    }

    private function canEdit(Error $error, User $user)
    {
        // this assumes that the data object has a getOwner() method
        // to get the entity of the user who owns this data object
        return strtoupper($user->getUsername()) === strtoupper($error->getCreatedBy());
    }

    ////private function canDelete(Error $error, User $user)
    private function canDelete(Error $error, User $user)
    {
        // this assumes that the data object has a getOwner() method
        // to get the entity of the user who owns this data object
        return strtoupper($user->getUsername()) === strtoupper($error->getCreatedBy());
    }

}