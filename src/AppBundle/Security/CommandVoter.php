<?php
/**
 * Created by PhpStorm.
 * User: rondra
 * Date: 16. 12. 2015
 * Time: 14:26
 */

namespace AppBundle\Security;


use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use AppBundle\Entity\Command;
use AppBundle\Entity\User;


class CommandVoter extends Voter
{
    const VIEW = 'View';
    const EDIT = 'Edit';
    const DELETE = 'Delete';

    private $decisionManager;

    public function __construct(AccessDecisionManagerInterface $decisionManager)
    {
        $this->decisionManager = $decisionManager;
    }

    protected function supports($attribute, $subject)
    {
        // if the attribute isn't one we support, return false
        if (!in_array($attribute, array(self::VIEW, self::EDIT, self::DELETE))) {
            return false;
        }

        // only vote on Post objects inside this voter
        if (!$subject instanceof Command) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        // ROLE_SUPER_ADMIN can do anything! The power!
        if ($this->decisionManager->decide($token, array('ROLE_SUPER_ADMIN'))) {
            return true;
        }

        if (!$user instanceof User) {
            // the user must be logged in; if not, deny access
            return false;
        }

        // you know $subject is a Command object, thanks to supports
        /** @var Command $command */
        $command = $subject;

        switch($attribute) {
            case self::VIEW:
                return $this->canView($command, $user);
            case self::EDIT:
                return $this->canEdit($command, $user);
            case self::DELETE:
                return $this->canDelete($command, $user);
        }
    }

    private function canView(Command $command, User $user)
    {
        // if they can edit, they can view
        if ($this->canEdit($command, $user)) {
            return true;
        }

        // the Post object could have, for example, a method isPrivate()
        // that checks a boolean $private property
        return !$command->isActive();
    }

    private function canEdit(Command $command, User $user)
    {
        // this assumes that the data object has a getOwner() method
        // to get the entity of the user who owns this data object
        return $user === $command->getCreatedBy();
    }

    private function canDelete(Command $command, User $user)
    {
        // this assumes that the data object has a getOwner() method
        // to get the entity of the user who owns this data object
        return $user === $command->getCreatedBy();
    }

}