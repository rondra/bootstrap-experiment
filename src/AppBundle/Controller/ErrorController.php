<?php

namespace AppBundle\Controller;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\HeaderBag;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Doctrine\ORM\QueryBuilder;

use AppBundle\Entity\Error;
use AppBundle\Repository\ErrorRepository;
use AppBundle\Form\ErrorType;

use AppBundle\Json;
/**
 * Error controller.
 *
 * @Route("/error")
 */
class ErrorController extends Controller
{
    /**
     * Lists all Error entities.
     *
     * @Route("/", name="error")
     * @Route("/index")
     * @Method("GET")
     * @Template
     */
    public function indexAction()
    {
        return array();
    }

    /**
     * Lists all Error entities.
     *
     * @Route("/get_json", name="get_json", defaults={"_format"="json"})
     * @Method("GET")
     * @Template("AppBundle:Error:index.json.twig")
     */
    public function getJsonAction(Request $request)
    {
        $alias = 'error';
        $qb = $this->getRepository()->createQueryBuilder($alias);

        /** @var int  $total */
        $total = 0;
        $this->getRepository()->applyRequestLimits($qb, $request, $alias, $total);

        /** @var \AppBundle\Entity\Error[]  $result */
        $result = $qb->getQuery()->getResult();
        return array('total'=> $total, 'entities' => $result);
    }

    /**
     * Creates a new Error entity.
     *
     * @Route("/create", name="error_create")
     * @Method("POST")
     * @Template("AppBundle:Error:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Error();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('error_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Error entity.
     *
     * @param Error $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Error $entity)
    {
        $form = $this->createForm(new ErrorType(), $entity, array(
            'action' => $this->generateUrl('error_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Error entity.
     *
     * @Route("/new", name="error_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Error();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Error entity.
     *
     * @Route("/{id}", name="error_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Error')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Error entity.');
        }

        if (false === $this->get('security.authorization_checker')->isGranted('view', $entity)) {
            throw new AccessDeniedException('Unauthorised access!');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Error entity.
     *
     * @Route("/{id}/edit", name="error_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Error')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Error entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Creates a form to edit a Error entity.
     *
     * @param Error $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Error $entity)
    {
        $form = $this->createForm(new ErrorType(), $entity, array(
            'action' => $this->generateUrl('error_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Error entity.
     *
     * @Route("/{id}", name="error_update")
     * @Method("PUT")
     * @Template("AppBundle:Error:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Error')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Error entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('error_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Error entity using modal confirmation window.
     *
     * @Route("/deleteModal", name="error_delete_modal")
     * @Method("POST")
     */
    public function deleteModalAction(Request $request)
    {
        $selections = $request->request->get('selections');

        foreach ($selections as $selection)
        {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Error')->find($selection['e_Id']);
            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Error entity.');
            }

            $em->remove($entity);
        }

        $em->flush();

        /*$form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Error')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Error entity.');
            }

            $em->remove($entity);
            $em->flush();
        }
        */

        return $this->redirect($this->generateUrl('error'));
    }

    /**
     * Creates a new Error entity.
     *
     * @Route("/delete", name="error_delete")
     * @Method("POST")
     * @Template("AppBundle:Error:delete.html.twig")
     */
    public function deleteAction(Request $request)
    {
        $entity = new Error();
        $selections = $request->request->get('selections');
        $form = $this->createDeleteForm($selections);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('error'));
        }

        return array(
            'entity' => $entity,
            'selections' => json_decode($selections),
            'form'   => $form->createView(),
        );
    }


    /**
     * Creates a form to delete a Error entity by id.
     *
     * @param mixed $selections Selected entities
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($selections)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('error_delete'))
            ->setMethod('POST')
            ->add('selections', 'hidden', array('data' => json_decode($selections, true)))
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
            ;
    }

    public function getRequest()
    {
        return $this->container->get('request_stack')->getCurrentRequest();
    }

    /**
     * @return ErrorRepository
     */
    private function getRepository()
    {
        $em = $this->getDoctrine()->getManager();
        return $em->getRepository('AppBundle:Error');
    }

}
