<?php
/**
 * Created by PhpStorm.
 * User: rondra
 * Date: 20. 12. 2015
 * Time: 12:29
 */

namespace AppBundle\Entity;

trait AuditTrait
{
    /**
     * @ORM\Column(name="ID", type="integer")
     * @ORM\Id
     */
    protected $id;

    /**
     * @ORM\Column(name="CREATED_BY", type="string", length=100)
     */
    protected $createdBy;

    /**
     * @ORM\Column(name="CREATED_BY_ID", type="integer", length=18)
     */
    protected $createdById;

    /**
     * @ORM\Column(name="DATE_CREATED", type="datetime")
     */
    protected $dateCreated;

    /**
     * @ORM\Column(name="MODIFIED_BY", type="string", length=100)
     */
    protected $modifiedBy;

    /**
     * @ORM\Column(name="MODIFIED_BY_ID", type="integer", length=18)
     */
    protected $modifiedById;

    /**
     * @ORM\Column(name="DATE_MODIFIED", type="datetime")
     */
    protected $dateModified;

    /**
     * @ORM\Column(name="IS_ACTIVE", type="integer", length=1)
     */
    protected $isActive;

    /**
     * Set id
     *
     * @param integer $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdBy
     *
     * @param string $createdBy
     *
     * @return $this
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return string
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set createdById
     *
     * @param integer $createdById
     *
     * @return $this
     */
    public function setCreatedById($createdById)
    {
        $this->createdById = $createdById;

        return $this;
    }

    /**
     * Get createdById
     *
     * @return integer
     */
    public function getCreatedById()
    {
        return $this->createdById;
    }

    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     *
     * @return $this
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * Set modifiedBy
     *
     * @param string $modifiedBy
     *
     * @return $this
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    /**
     * Get modifiedBy
     *
     * @return string
     */
    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }

    /**
     * Set modifiedById
     *
     * @param integer $modifiedById
     *
     * @return $this
     */
    public function setModifiedById($modifiedById)
    {
        $this->modifiedById = $modifiedById;

        return $this;
    }

    /**
     * Get modifiedById
     *
     * @return integer
     */
    public function getModifiedById()
    {
        return $this->modifiedById;
    }

    /**
     * Set dateModified
     *
     * @param \DateTime $dateCreated
     *
     * @return $this
     */
    public function setDateModified($dateModified)
    {
        $this->dateModified = $dateModified;

        return $this;
    }

    /**
     * Get dateModified
     *
     * @return \DateTime
     */
    public function getDateModified()
    {
        return $this->dateModified;
    }

    /**
     * Set isActive
     *
     * @param integer $isActive
     *
     * @return $this
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return integer
     */
    public function getIsActive()
    {
        return $this->isActive;
    }
}