<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Entity;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ErrorRepository")
 * @ORM\Table(name="V_ERRORS")
 */

class Error
{
    use AuditTrait;

    /**
     * @ORM\Column(name="ERROR_NR", type="integer")
     */
    protected $errorNumber;

    /**
     * @ORM\Column(name="PROGRAM_NAME", type="string", length=30)
     */
    protected $programName;

    /**
     * @ORM\Column(name="TIME_CREATED", type="datetime")
     */
    protected $timeCreated;

    /**
     * @ORM\Column(name="ERROR_TEXT", type="string", length=512)
     */
    protected $errorText;

    /**
     * @ORM\Column(name="PROGRAM_VERSION", type="string", length=10)
     */
    protected $programVersion;

    /**
     * @ORM\Column(name="ERROR_STACK", type="string", length=4000)
     */
    protected $errorStack;

    /**
     * @ORM\Column(name="ROLE_CODES", type="string", length=4000)
     */
    protected $roles;

    /**
     * Set errorNumber
     *
     * @param string $errorNumber
     *
     * @return Error
     */
    public function setErrorNumber($errorNumber)
    {
        $this->errorNumber = $errorNumber;

        return $this;
    }

    /**
     * Get errorNumber
     *
     * @return string
     */
    public function getErrorNumber()
    {
        return $this->errorNumber;
    }

    /**
     * Set programName
     *
     * @param string $programName
     *
     * @return Error
     */
    public function setProgramName($programName)
    {
        $this->programName = $programName;

        return $this;
    }

    /**
     * Get programName
     *
     * @return string
     */
    public function getProgramName()
    {
        return $this->programName;
    }

    /**
     * Set timeCreated
     *
     * @param \DateTime $timeCreated
     *
     * @return Error
     */
    public function setTimeCreated($timeCreated)
    {
        $this->timeCreated = $timeCreated;

        return $this;
    }

    /**
     * Get timeCreated
     *
     * @return \DateTime
     */
    public function getTimeCreated()
    {
        return $this->timeCreated;
    }

    /**
     * Set errorText
     *
     * @param string $errorText
     *
     * @return Error
     */
    public function setErrorText($errorText)
    {
        $this->errorText = $errorText;

        return $this;
    }

    /**
     * Get errorText
     *
     * @return string
     */
    public function getErrorText()
    {
        return $this->errorText;
    }

    /**
     * Set programVersion
     *
     * @param string $programVersion
     *
     * @return Error
     */
    public function setProgramVersion($programVersion)
    {
        $this->programVersion = $programVersion;

        return $this;
    }

    /**
     * Get programVersion
     *
     * @return string
     */
    public function getProgramVersion()
    {
        return $this->programVersion;
    }

    /**
     * Set errorStack
     *
     * @param string $errorStack
     *
     * @return Error
     */
    public function setErrorStack($errorStack)
    {
        $this->errorStack = $errorStack;

        return $this;
    }

    /**
     * Get errorStack
     *
     * @return string
     */
    public function getErrorStack()
    {
        return $this->errorStack;
    }

    /**
     * Get roles
     *
     * @return string
     */
    public function getRoles()
    {
        return $this->roles;
    }
}
